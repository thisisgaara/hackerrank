# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 15:36:46 2015

@author: jill
def get_all_substrings(string, length):"""

count = 0
def number_of_substrings(n):
    return n * (n + 1) /2;
    
def remove_values_from_list(the_list, val):
    return [value for value in the_list if value != val]
    
def get_all_substrings(string, length):
    global count
    string = list(string)
    for i in string:
        take_count = string.count(i)
        string = remove_values_from_list(string, i)
        if(take_count > 1):
            count += number_of_substrings(take_count - 1)
    return count + length
    
length = input()
string = str(raw_input())
print get_all_substrings(string, length);