-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: python-defaults
Binary: python, python-minimal, python-examples, python-dev, idle, python-doc, python-dbg, python-all, python-all-dev, python-all-dbg
Architecture: any all
Version: 2.7.3-0ubuntu2.2
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders: Scott Kitterman <scott@kitterman.com>, Piotr Ożarowski <piotr@debian.org>
Homepage: http://www.python.org/
Standards-Version: 3.9.3
Vcs-Browser: http://alioth.debian.org/scm/loggerhead/pkg-python/python-defaults-debian/files
Vcs-Bzr: http://alioth.debian.org/anonscm/bzr/pkg-python/python-defaults-debian
Build-Depends: debhelper (>= 6.0.7~), lsb-release, libhtml-tree-perl, debiandoc-sgml, python-docutils (>= 0.4-3)
Package-List: 
 idle deb python optional
 python deb python important
 python-all deb python optional
 python-all-dbg deb debug extra
 python-all-dev deb python optional
 python-dbg deb debug extra
 python-dev deb python optional
 python-doc deb doc optional
 python-examples deb python optional
 python-minimal deb python required
Checksums-Sha1: 
 041b353e1dcab03e1d6f4e8b8bb4285e47fe3acd 163342 python-defaults_2.7.3-0ubuntu2.2.tar.gz
Checksums-Sha256: 
 3a83c3fc9ea6441eca688a141aa32dd2ffb3c5b4d67d05f9026a8e1d88b473a9 163342 python-defaults_2.7.3-0ubuntu2.2.tar.gz
Files: 
 3866dc550444c7b855190ebaf4abc9d2 163342 python-defaults_2.7.3-0ubuntu2.2.tar.gz
Original-Maintainer: Matthias Klose <doko@debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJRv1DoAAoJEBJutWOnSwa/0o0P+wQkXKWAxVSW0khG3DnWsdOG
g/wdhWbTZARTWn8DWFJFsbPggDEToHjyZt7nOCooVRvnv+oXl5+pEZxXHtBBAy+g
1ge7Pj/uDkxIFCE77h9WhdzG3u9BDegmCiR7umWh/8MSEd41wx1APXI0tR50Yhhx
KPClsztzxtEUjg4SYH0a8UddIo87OMWWIFIEceCiXJ1/6vxhWQ3tJ9WKkHFQIJeY
sqYyC3svosg7I8a8Hjeb9jWYaz/nCzJJdtZkK7cZFyhSn5ATVZ0nLXBR1shZqdpj
5g74ihADtHZjzaiieQ92bGVhS1GrPAVdYnzq/uzvEPB74rgQqu6fkP+mO2QBlBTH
Hn/GFoaez5if4Cs1KjRr0WRi9SLHNcg+EIWQxn3BxFL1Yw/s58LFKcBUS2DKixlS
UCLgRIdS4xAC0twwvao+HY2C3MLTjoPJhX69vrbv6FUs/j+3RFxwrDzQLJARQZ5h
hSQMWDfKcUAz2bxqoO90/tnCOel2eikBx9W362hdXPzubIe7rnkQ4HMPV8ktTSXT
dUBPZP4gu21s77mAsZaqoEqhKcELkfsm0XB4aaK+Qx/0Y1mlL20V9+R5xhnodtGE
uUz/INE4XHe76eh0FVimy6W/MybrE6u09EjjlZ7jaUWNUhv45/mgZUyri9eCZ2wD
qiSjX6Kh4EytlSgVPsJP
=Mwwo
-----END PGP SIGNATURE-----
