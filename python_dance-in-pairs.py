# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 17:16:09 2015

@author: jill
"""

count = 0
N = raw_input()
N = N.split()
N, K = map(int, N)

boys = raw_input()
boys = boys.split()
boys = map(int, boys)

girls = raw_input()
girls = girls.split()
girls = map(int, girls)

boys = sorted(boys)
girls = sorted(girls)

boys_copy = boys[:]

best_diff = K;
best_diff_girl = -1

flag = 0

pairing = []
"""
for i, j in itertools.izip(boys_copy, girls_copy):
    if abs(i - j) <= K:
        boys.remove(i)
        girls.remove(j)                           
        count += 1
"""


for i in girls:
    for j in boys:          
        if (abs (i - j) <= K):
            pairing.append(j)
            flag = 1
        else:
            break;
    if flag == 1:
        mini = min(pairing)    
        boys.remove(mini)    
        count += 1
        flag = 0;
        pairing = []        
print count