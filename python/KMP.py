# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 11:11:56 2015
@author: jill
Source: https://www.youtube.com/watch?v=2ogqPWJSftE

"""

def string_find(text, prefix, pattern):
    m = len(pattern);
    n = len(text)
    i = 0
    j = 0
    while(i < n):
        if(text[i] == pattern[j]):
            i+=1
            j+=1
        if j == m:
            print i - j
            j = prefix[j -1]
        elif i < n and text[i] != pattern[j]:
            if(j != 0):
                j = prefix[j -1]
            else:
                i += 1
                
def prefix_search(p):
    prefix = [-1 for i in p];
    a = -1;
    for b in range(1, len(p)):
        if (a >= 0) and (p[a+1] != p[b]):
            a = prefix[a-1];
        if(p[a+1] == p[b]):
            a = a + 1;
        prefix[b] = a;
    return [i+1 for i in prefix]
    

if __name__ == "__main__":
    text = "cozacocacolacococacola"
    pattern = "cocacola";
    prefix = prefix_search(pattern);
    string_find(text, prefix, pattern);