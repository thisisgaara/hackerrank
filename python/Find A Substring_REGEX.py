# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 15:08:19 2015

@author: jill
"""

import re;
flag = [0 for i in range(0, 10)]
def find_substring(temp, check):
    temp = temp.split();
    global flag
    for i in temp:
        for j in range(0, len(check)):
            if check[j]:
                regex = re.compile("[\w\d_]+"+ check[j] +"[\w\d_]+");
                flag[j] += len(re.findall(regex, i))
if __name__ == "__main__":
    N = int(raw_input());
    global flag
    temp = [0 for i in range(0, 100)]
    for i in range(0, N):
        temp[i] = str(raw_input());
    T = int(raw_input());
    check = [0 for i in range(0, 10)]
    for i in range(0, T):
        check[i] = str(raw_input());
    for i in range(0, N):
        find_substring(temp[i], check);
    for i in range(0, T):
            print flag[i];
        

