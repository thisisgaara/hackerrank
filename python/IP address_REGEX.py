# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 10:20:46 2015

@author: jill
"""

temp = [];
Flag_ipv4 = 0;
import re;

def check(temp):
    global Flag_ipv4;
    if (not check_ipv4(temp)):
        if(not check_ipv6(temp)):
            print "Neither";
            return;
    if(Flag_ipv4):
        print "IPV4";
    else:
        print "IPV6";
    Flag_ipv4 = 0; #Reset here
    
def check_ipv6(temp):
    IPV6 = re.compile("^(?:[a-fA-F0-9]{1,4}:){7}[a-fA-F0-9]{1,4}$");
    value = IPV6.match(temp);
    if (value == None):
        return 0;
    else:
        return 1;
    
def check_ipv4(temp):
    global Flag_ipv4;
    IPV4 = re.compile("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
    value = IPV4.match(temp);
    if (value == None):
        Flag_ipv4 = 0;
    else:
        Flag_ipv4 = 1;
    return Flag_ipv4;
    
    
if __name__ == "__main__":
    num = int(raw_input())
    for i in  range(0, num):
        temp = str(raw_input());
        check(temp)
    