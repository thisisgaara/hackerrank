# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 09:11:48 2015

@author: jill

Concept:
    Student            Handshake
    1                   0
    2                   1 + 0
    3                   2 + 1
    4                   3 + 2 + 1
    5                   4 + 3 + 2+ 1
    
Overall: Sumamtion of (n -1) items
"""


def calculate_handshake(num):
    print (num * (num - 1) /2);

if __name__ == "__main__":
    T = int(raw_input());
    while(T):
        num = int(raw_input());
        calculate_handshake(num);        
        T-=1