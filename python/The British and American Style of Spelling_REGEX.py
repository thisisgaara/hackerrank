# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 17:08:41 2015

@author: jill
"""
import re;
flag = [0 for i in xrange(10)];
def british_us_style(temp, check, T):
    global flag
    temp = temp.split();
    for i in temp:
        for j in range(0,T):
            lm= check[j]
            k = lm[0:len(check[j])-2]
            regex = re.compile(k+"[se|ze]+$");
            if(re.match(regex, i)):
                flag[j] += 1;
                
if __name__ == "__main__":
    N = int(raw_input());
    global flag
    temp = [0 for i in range(0, N)]
    for i in range(0, N):
        temp[i] = str(raw_input());
    T = int(raw_input());
    check = [0 for i in range(0, 10)]
    for i in range(0, T):
        check[i] = str(raw_input());
    for i in range(0, N):
        british_us_style(temp[i], check, T);
    for i in range(0, T):
        print flag[i];
        