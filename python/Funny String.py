# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:17:46 2015

@author: jill
"""

def check_funny_string(ip, ip_rev, length):
    flag = 1;    
    for i in range(0, length-1):
        if(abs(ord(ip[i]) - ord(ip[i+1])) - abs(ord(ip_rev[i]) - ord(ip_rev[i+1])) != 0):
            flag = 0;
            break;
    if(flag):
        print "Funny\n";
    else:
        print "Not Funny\n";
             

if __name__ == "__main__":
    num = int(raw_input())
    while(num):    
        num -= 1
        ip = raw_input();
        ip_rev = ip[::-1];
        check_funny_string(ip, ip_rev,len(ip));
    