# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 21:33:44 2015

@author: jill
"""
import math;
temp = [0 for i in range(0, 1000)] #This is the index of prime numbers
index = 0;
def check_prime(num):
        temp = math.ceil(num / 2);
        if(num /2 == 0):
            return 0;
        for i in range(3, temp, 2):
            if(num / i == 0):
                return 0;
        return 1;

def find_prime_numbers_below_1000(num):
    global temp, index
    temp.append(2);
    for i in range(3, 1000, 2):
        flag = check_prime(i);
        if flag == 1:
            temp[index] = i;
            index += 1;
        flag = 0;
    
def print_prime(temp):
    find_prime_numbers_below_1000();

if __name__ == "__main__":
    num = int (raw_input());
    while(num):
        temp = int(raw_input());
        print_prime(temp)
        
    