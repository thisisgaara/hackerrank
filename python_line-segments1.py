co_ord = [];
N = int(input())
temp = []
total_value = 0;
flag = 0
max_value = 0;
test_array = [0 for i in xrange(10**5)]

def double_check(co_ord, i, j):
    global test_array
    x = co_ord[i][0]
    y = co_ord[i][1]
    for j in xrange(0, i):
        if (x <= co_ord[j][0]  and y >= co_ord[j][1]):
            co_ord.remove([x,y])        
            return True
    test_array.append(j)
    return False

def check_dup(N):
    global test_array, total_value, co_ord
    for i in range(0, N+1):
        back_up_list = test_array[:];
        for j in range(co_ord[i][0], co_ord[i][1]+1):        
            test_array[j] = i + 1;
        for j in range(1, i + 2):
            if j not in test_array:
                if double_check(co_ord, i, j) == True:
                    test_array = back_up_list
                    flag = 1
                    break
        if flag == 0:
            total_value += 1
        flag = 0;
        
for i in xrange(N):
    temp = raw_input()
    temp = temp.split()
    temp = map(int, temp)   
    co_ord.append(temp);
    check_dup(len(co_ord))
    
print total_value
        
