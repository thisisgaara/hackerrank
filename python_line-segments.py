co_ord = [];
N = int(input())
temp = []

for i in xrange(N):
    global max_value
    temp = raw_input()
    temp = temp.split()
    temp = map(int, temp)  
    co_ord.append(temp);

co_ord.sort(key = lambda co_ord: co_ord[1])
i = 1
while(i < N):
    current_activity = co_ord[i]
    prev_activity = co_ord[i-1]   
    if (current_activity[1] >= prev_activity[1] and current_activity[0] <= prev_activity[0]):
        co_ord.remove(current_activity)
        N -= 1        
        continue;
    i += 1
    
print N